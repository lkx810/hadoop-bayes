package com.kaixi.classifier;

import com.kaixi.classifier.helper.ClassiferToken;
import com.kaixi.classifier.helper.ClassiferTokenArrayWritable;
import com.kaixi.classifier.helper.WeightTokenArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

/**
 * MapReduce Job Driver
 *
 * @author Kaixili
 * @version 2018.12
 * @see NaiveBayesClassify http://en.wikipedia.org/wiki/Naive_Bayes_classifier
 * Copyright 2018 MIT License
 */
public class MainClass {
    private static final Logger parentLogger = LogManager.getLogger();

    private static Configuration[] conf = null;
    private static String jar = null;

    public static void main(String[] args) throws Exception {
        buildConf(args);
        checkFile(getConf()[0]);
        preprocess(getConf()[0], jar);
        classify(getConf()[1], jar);
    }

    /**
     * 检查输入格式，并计算先验概率
     *
     * @param conf global configuration
     * @throws Exception raise exception if input wrong fromat
     */
    private static void checkFile(Configuration conf) throws Exception {
        FileSystem fs = FileSystem.get(conf);

        FileStatus[] dirStatus = fs.listStatus(new Path(conf.get("InputTrainLocation")));
        Map<String, Integer> map = new Hashtable<>();
        for (FileStatus dirStat : dirStatus) {
            if (!dirStat.isDirectory())
                throw new IOException("Wrong Input Format: " +
                        "file should be in a category directory.");
            FileStatus[] fileStatus = fs.listStatus(dirStat.getPath());
            for (FileStatus fileStat : fileStatus) {
                if (fileStat.isDirectory())
                    throw new IOException("Wrong Input Format: " +
                            "Recursion is not allowed.");
                map.merge(dirStat.getPath().getName(), 1, (a, b) -> a + b);
            }
        }
        int sum = map.entrySet().stream().mapToInt(Map.Entry::getValue).sum();
        System.out.println("Input file numbers: " + map);
        System.out.println("Sum: " + sum);
        parentLogger.info("input file numbers: " + map);
        SequenceFile.Writer writer = SequenceFile.createWriter(
                conf,
                SequenceFile.Writer.file(new Path(conf.get("PriorProbLocation"))),
                SequenceFile.Writer.keyClass(Text.class),
                SequenceFile.Writer.valueClass(DoubleWritable.class)
        );
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            writer.append(
                    new Text(entry.getKey()),
                    new DoubleWritable(Math.log10((double) entry.getValue() / sum))
            );
        }
        writer.close();
    }

    /**
     * 训练过程，输出统计单词数
     *
     * @param conf global configuration
     * @param jar jar location for Eclipse & IDEA.
     *            Null if upload the Jar to the server.
     * @throws Exception if job failed raise exception.
     */
    private static void preprocess(Configuration conf, String jar) throws Exception {
        System.out.println("Train Start.");
        Job job = Job.getInstance(conf, "BayesTrain");
        if (jar != null) {
            // see below, use this when submitting from Eclipse & IDEA.
            // use arg indicate local build jar location.
            job.setJar(jar);
        } else {
            // use this when uploaded the Jar to the server
            // and running the job directly and locally on the server.
            job.setJarByClass(NaiveBayesPreproccess.class);
        }
        job.setMapperClass(NaiveBayesPreproccess.PreproccessMapper.class);
        // job.setCombinerClass(PreproccessReducer.class);
        job.setReducerClass(NaiveBayesPreproccess.PreproccessReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(ClassiferToken.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(ClassiferTokenArrayWritable.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        FileInputFormat.addInputPath(job, new Path(conf.get("InputTrainLocation")));
        FileOutputFormat.setOutputPath(job, new Path(conf.get("OutputLocation")));
        if (!job.waitForCompletion(true))
            throw new Exception("Something Wrong, Please Check Yarn Log!");
    }

    private static void classify(Configuration conf, String jar) throws Exception {
        System.out.println("Classify Start.");
        Job job = Job.getInstance(conf, "BayesTest");
        if (jar != null) {
            job.setJar(jar);
        } else {
            job.setJarByClass(NaiveBayesClassify.class);
        }
        job.setMapperClass(NaiveBayesClassify.ClassifyMapper.class);
        // job.setCombinerClass(PreproccessReducer.class);
        job.setReducerClass(NaiveBayesClassify.ClassifyReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(WeightTokenArrayWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(conf.get("InputTestLocation")));
        FileOutputFormat.setOutputPath(job, new Path(conf.get("ResultLocation")));
        if (!job.waitForCompletion(true))
            throw new Exception("Something Wrong, Please Check Yarn Log!");
    }

    private static void buildConf(String[] args) {
        if (System.getProperty("HADOOP_USER_NAME") == null)
            System.setProperty("HADOOP_USER_NAME", "root"); // fix server hadoop permission
        conf = new Configuration[2];
        jar = args.length != 1 ? null : args[0];
        for (int i = 0; i < conf.length; i++) {
            conf[i] = new Configuration();
            conf[i].set("InputTrainLocation", "/user/root/input/train");
            conf[i].set("InputTestLocation", "/user/root/input/test");

            conf[i].set("PriorProbLocation", "/user/root/output/model");
            conf[i].set("PreprocessLocation", "/user/root/output/output/part-r-00000");
            conf[i].set("OutputLocation", "/user/root/output/output");

            conf[i].set("ResultLocation", "/user/root/output/result");
        }
    }

    private static Configuration[] getConf() {
        return conf;
    }
}
