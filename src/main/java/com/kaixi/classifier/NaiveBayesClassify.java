package com.kaixi.classifier;

import com.kaixi.classifier.helper.ClassiferToken;
import com.kaixi.classifier.helper.ClassiferTokenArrayWritable;
import com.kaixi.classifier.helper.WeightToken;
import com.kaixi.classifier.helper.WeightTokenArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.IOException;
import java.util.*;

/**
 * 贝叶斯分类器
 * 在 @ClassifyMapper 依据多项式公式，构建条件概率，输出文档中每个单词的条件概率
 * 在 @ClassifyReducer 构建先验概率，并计算条件概率乘积，得到贝叶斯概率，
 *              输出对应贝叶斯概率最大的类别
 *
 * @author Kaixili
 * @version 2018.12
 * @see NaiveBayesClassify http://en.wikipedia.org/wiki/Naive_Bayes_classifier
 *
 * Copyright 2018 MIT License
 */
public class NaiveBayesClassify {
    public static class ClassifyMapper
            extends Mapper<Object, Text, Text, ArrayWritable>{
        /** 条件概率 */
        private Map<String, Map<String, Double>> featureConditionalProb
                = new Hashtable<>();
        /** 没有出现的词的条件概率 */
        private Map<String, Double> absentFeatureProb = new Hashtable<>();

        /**
        * 从文件读取预处理单词数，使用多向式模型构造条件概率
        */
        @Override
        public void setup(Context context) throws IOException {
            Configuration conf = context.getConfiguration();
            String preprocessFile = conf.get("PreprocessLocation");

            SequenceFile.Reader sequenceFileReader = new SequenceFile.Reader(conf,
                    SequenceFile.Reader.file(new Path(preprocessFile))
                    );
            Text key = (Text) ReflectionUtils.newInstance(
                    sequenceFileReader.getKeyClass(), conf);
            ClassiferTokenArrayWritable value =
                    (ClassiferTokenArrayWritable) ReflectionUtils.newInstance(
                    sequenceFileReader.getValueClass(), conf);

            // 计算多项式模型中的B (不同单词数)
            int totalFeatureNum = 0;
            try {
                while (sequenceFileReader.next(key, value)) totalFeatureNum++;
            } finally {
                sequenceFileReader = new SequenceFile.Reader(conf,
                        SequenceFile.Reader.file(new Path(preprocessFile)));
            }
            // 构造条件概率
            try {
                while (sequenceFileReader.next(key, value)) {
                    ClassiferToken[] values = (ClassiferToken[]) value.get();
                    double sum = values[values.length - 1].getnum().get();
                    for (int i = 0; i < values.length - 1; i++) {
                        ClassiferToken val = values[i];
                        String category = val.getCategory().toString();
                        int weight = val.getnum().get();
                        featureConditionalProb.computeIfAbsent(
                                category,
                                k -> new Hashtable<>()
                        ).put(key.toString(),
                                Math.log10((double)(weight + 1) / (sum + totalFeatureNum)));

                        absentFeatureProb.put(
                                category,
                                Math.log10((double)(1) / (sum + totalFeatureNum)));
                    }
                }
            } finally {
                IOUtils.closeStream(sequenceFileReader);
            }
        }

        @Override
        public void map(Object object, Text value, Context context)
                throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            Text fileName = new Text(getFileName(context));
            while (itr.hasMoreTokens()) {
                String feature = itr.nextToken();
                List<WeightToken> weightTokens = new ArrayList<>();
                for (String category : featureConditionalProb.keySet()) {
                    weightTokens.add(new WeightToken(
                            category,
                            featureConditionalProb
                                    .get(category)
                                    .getOrDefault(feature, absentFeatureProb.get(category))
                            )
                    );
                }
                WeightToken[] output = new WeightToken[weightTokens.size()];
                output = weightTokens.toArray(output);
                // 输出 [文档名, [类别, 权重(条件概率)]…]
                context.write(fileName, new WeightTokenArrayWritable(output));
            }
        }

        private String getFileName(Context context) {
            String[] splits = ((FileSplit) context.getInputSplit())
                    .getPath().toString().split("/");
            return splits[splits.length - 2] + " / " + splits[splits.length - 1];
        }
    }

    public static class ClassifyReducer
            extends Reducer<Text, WeightTokenArrayWritable, Text, Text> {

        /** 先验概率 */
        private Map<String, Double> priorProb = new Hashtable<>();

        /**
        * 从文件读取先验概率
        * */
        @Override
        public void setup(Reducer.Context context) throws IOException {
            Configuration conf = context.getConfiguration();
            String priorProbFile = conf.get("PriorProbLocation");

            SequenceFile.Reader priorProbReader = new SequenceFile.Reader(conf,
                    SequenceFile.Reader.file(new Path(priorProbFile)));
            Text category = (Text) ReflectionUtils.newInstance(
                    priorProbReader.getKeyClass(), conf);
            DoubleWritable categoryNum = (DoubleWritable) ReflectionUtils.newInstance(
                    priorProbReader.getValueClass(), conf);
            try {
                while (priorProbReader.next(category, categoryNum)) {
                    priorProb.put(category.toString(), categoryNum.get());
                }
            } finally {
                IOUtils.closeStream(priorProbReader);
            }
        }

        @Override
        public void reduce(Text key, Iterable<WeightTokenArrayWritable> values,
                           Context context) throws IOException, InterruptedException {
            Map<String, Double> weightSum = new HashMap<>();
            // 计算属于不同类别的条件概率乘积（log计算为加）
            for (WeightTokenArrayWritable val : values) {
                for (Writable token : val.get()) {
                    String category = ((WeightToken)token).getCategory().toString();
                    double weight = ((WeightToken)token).getWeight().get();
                    weightSum.merge(category, weight, (a, b) -> a + b);
                }
            }
            // 乘以先验概率得到贝叶斯概率 (log计算为加)
            weightSum.replaceAll((k, v) -> v + priorProb.get(k));
            // 得到最大概率对应的类别
            Text category = weightSum.entrySet().stream()
                    .max(Comparator.comparing(Map.Entry::getValue))
                    .map(Map.Entry::getKey)
                    .map(Text::new)
                    .orElse(null);
            // 输出 [文档名, 类别]
            context.write(key, category);
        }
    }

}