package com.kaixi.classifier.helper;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * WeightToken
 * Pair\<category, weight\> (\<String, Double\>)
 *
 * @author Kaixili
 * @version 2018.12
 *
 * Copyright 2018 MIT License
 */
public class WeightToken implements WritableComparable<WeightToken> {
    private Text category;
    private DoubleWritable weight;

    public WeightToken() {
        this.category = new Text();
        this.weight = new DoubleWritable();
    }

    public WeightToken(WeightToken ClassiferToken) {
        this.category = new Text(ClassiferToken.getCategory());
        this.weight = new DoubleWritable(ClassiferToken.getWeight().get());
    }
    
    public WeightToken(String classify, double token) {
        this.category = new Text(classify);
        this.weight = new DoubleWritable(token);
    }

    public Text getCategory() {
        return category;
    }

    public void setCategory(Text featureset) {
        this.category = featureset;
    }

    public DoubleWritable getWeight() {
        return weight;
    }

    public void setWeight(DoubleWritable num) {
        this.weight = num;
    }
    public void set(Text featureset, DoubleWritable num){
        this.category=featureset;
        this.weight=num;
    }

    @Override
    public int hashCode() {
        return category.hashCode() * 163 + weight.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof WeightToken){
            WeightToken tp=(WeightToken)obj;
            return category.equals(tp.getCategory()) && weight.equals(tp.getWeight());
        }
        return false;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return category+"\t"+weight;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        // TODO Auto-generated method stub
        category.readFields(in);
        weight.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        // TODO Auto-generated method stub
        category.write(out);
        weight.write(out);
    }

    @Override
    public int compareTo(WeightToken tp) {
        // TODO Auto-generated method stub
        int cmp=category.compareTo(tp.getCategory());
        if (cmp != 0) return cmp;
        return weight.compareTo(tp.getWeight());

    }
}