package com.kaixi.classifier.helper;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableFactories;

import java.io.DataInput;
import java.io.IOException;

/**
 * WeightTokenArrayWritable
 *
 * @author Kaixili
 * @version 2018.12
 *
 * Copyright 2018 MIT License
 */
public class WeightTokenArrayWritable extends ArrayWritable {
    public WeightTokenArrayWritable() { super(WeightToken.class); }

    public WeightTokenArrayWritable(WeightToken[] values) {
        this();
        this.set(values);
    }

    public void readFields(DataInput in) throws IOException {
        WeightToken[] values = new WeightToken[in.readInt()];

        for(int i = 0; i < values.length; ++i) {
            Writable value = WritableFactories.newInstance(WeightToken.class);
            value.readFields(in);
            values[i] = (WeightToken) value;
        }
        this.set(values);
    }
}
