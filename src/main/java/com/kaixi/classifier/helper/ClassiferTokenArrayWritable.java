package com.kaixi.classifier.helper;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableFactories;

import java.io.DataInput;
import java.io.IOException;

/**
 * ClassiferTokenArrayWritable
 *
 * @author Kaixili
 * @version 2018.12
 *
 * Copyright 2018 MIT License
 */
public class ClassiferTokenArrayWritable extends ArrayWritable {
    public ClassiferTokenArrayWritable() { super(ClassiferToken.class); }

    public ClassiferTokenArrayWritable(Writable[] values) {
        this();
        this.set(values);
    }

    public void readFields(DataInput in) throws IOException {
        Writable[] values = new ClassiferToken[in.readInt()];

        for(int i = 0; i < values.length; ++i) {
            Writable value = WritableFactories.newInstance(ClassiferToken.class);
            value.readFields(in);
            values[i] = value;
        }
        this.set(values);
    }
}
