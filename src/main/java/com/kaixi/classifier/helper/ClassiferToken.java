package com.kaixi.classifier.helper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * WeightToken
 * Pair\<category, num\> (\<String, Integer\>)
 *
 * @author Kaixili
 * @version 2018.12
 *
 * Copyright 2018 MIT License
 */
public class ClassiferToken implements WritableComparable<ClassiferToken> {
    private Text category;
    private IntWritable num;

    public ClassiferToken() {
        this.category = new Text();
        this.num = new IntWritable();
    }

    public ClassiferToken(ClassiferToken ClassiferToken) {
        this.category = new Text(ClassiferToken.getCategory());
        this.num = new IntWritable(ClassiferToken.getnum().get());
    }

    public ClassiferToken(String classify, int token) {
        this.category = new Text(classify);
        this.num = new IntWritable(token);
    }

    public Text getCategory() {return category;}

    public void setClassify(Text category) {this.category = category;}

    public IntWritable getnum() {return num;}

    public void setToken(IntWritable num) {this.num = num;}
    public void set(Text category, IntWritable num){
    this.category=category;
        this.num=num;
    }

    @Override
    public int hashCode() {
        return category.hashCode() * 163 + num.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ClassiferToken){
            ClassiferToken tp=(ClassiferToken)obj;
            return category.equals(tp.getCategory()) && num.equals(tp.getnum());
        }
        return false;
    }

    @Override
    public String toString() {return category+"\t"+num;}

    @Override
    public void readFields(DataInput in) throws IOException {
        category.readFields(in);
        num.readFields(in);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        category.write(out);
        num.write(out);
    }

    @Override
    public int compareTo(ClassiferToken tp) {
        int cmp=category.compareTo(tp.getCategory());
        if (cmp != 0) return cmp;
        return num.compareTo(tp.getnum());

    }
}