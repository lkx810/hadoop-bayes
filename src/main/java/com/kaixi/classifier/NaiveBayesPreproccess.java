package com.kaixi.classifier;
import com.kaixi.classifier.helper.ClassiferToken;
import com.kaixi.classifier.helper.ClassiferTokenArrayWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.util.*;

/**
 * 贝叶斯分类器预处理器（训练）
 * 在 @PreproccessMapper 分词
 * 在 @PreproccessReducer 统计每个词在不同分类下出现的次数，及总次数
 *
 * @author Kaixili
 * @version 2018.12
 * @see NaiveBayesClassify http://en.wikipedia.org/wiki/Naive_Bayes_classifier
 *
 * Copyright 2018 MIT License
 */
public class NaiveBayesPreproccess {

    public static class PreproccessMapper
            extends Mapper<Object, Text, Text, ClassiferToken>{
        @Override
        public void map(Object key, Text value, Context context)
                throws IOException, InterruptedException {
            String category = getClassifyName(context);
            StringTokenizer itr = new StringTokenizer(value.toString());
            while (itr.hasMoreTokens()) {
                // 输出 [ 单词, (分类, 1) ]
                context.write(new Text(itr.nextToken())
                        , new ClassiferToken(category, 1));
            }
        }

        private String getClassifyName(Context context) {
            String[] splits = ((FileSplit) context.getInputSplit())
                    .getPath().toString().split("/");
            return splits[splits.length - 2];
        }
    }

    public static class PreproccessReducer
            extends Reducer<Text, ClassiferToken, Text, ClassiferTokenArrayWritable> {
        @Override
        public void reduce(Text key, Iterable<ClassiferToken> values,
                           Context context) throws IOException, InterruptedException {
            Map<String, Integer> counterMap = new TreeMap<>();
            int sum = 0;
            for (ClassiferToken val : values) {
                int num = val.getnum().get();
                counterMap.put(
                        val.getCategory().toString(),
                        counterMap.getOrDefault(val.getCategory().toString(), 0)
                                + num);
                sum += num;
            }
            List<ClassiferToken> tokenList = new ArrayList<>();
            for (Map.Entry<String, Integer> entry : counterMap.entrySet()) {
                tokenList.add(new ClassiferToken(entry.getKey(), entry.getValue()));
            }
            ClassiferToken[] output = new ClassiferToken[tokenList.size() + 1];
            output = tokenList.toArray(output);
            // 在最后保存该单词出现的总次数
            output[tokenList.size()] = new ClassiferToken("_TOTAL_", sum);
            // 输出 [ 单词, ( (分类, 数量), (分类, 数量)… , ("_TOTAL_", 总单词数) ) ]
            context.write(key, new ClassiferTokenArrayWritable(output));
        }
    }
}